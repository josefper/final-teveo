# Final-TeVeO


# ENTREGA CONVOCATORIA MAYO

# ENTREGA DE PRÁCTICA

## Datos

* Nombre: José Fuhui Pérez Contreras
* Titulación: Ingeniería Telemática
* Cuenta en laboratorios: josefper
* Cuenta URJC: jf.perez.2021
* Video básico (url): https://youtu.be/3XgA_OWtx2w
* Video parte opcional (url): https://youtu.be/YVlrrLMP3qM
* Despliegue (url): josefperez.pythonanywhere.com
* Contraseñas: admin
* Cuenta Admin Site: admin

## Resumen parte obligatoria

## Lista partes opcionales

* Favicon: Implementación de un icono para la página web.
* Botón de regresar: Un botón para volver a la paǵina anterior en cada cámara (tanto estática como dinámica) para mayor comodidad.
* Test de Nº de Cámaras: se comprueba que el número de cámaras totales coincide con el número de cámaras utilizadas en el sistema.
* Mensajes en configuración: se envían mensajes dependiendo de la acción cometida en el apartado de identificación de sesión.
