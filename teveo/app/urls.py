from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.index),
    path('camaras', views.camaras),
    path('camaras/<str:ident>-dyn', views.show_dynamic),
    path('camaras/<str:ident>-dyn/img-htmx', views.img_htmx),
    path('camaras/<str:ident>-dyn/com-htmx', views.com_htmx),
    path('camaras/<str:ident>', views.show_camera, name='show_camera'),
    path('camaras/<str:ident>/json', views.get_json),
    path('camaras/<str:ident>/comentario', views.comentario),
    path('ayuda', views.helpme),
    path('config', views.config),
]
