# Generated by Django 3.2.12 on 2024-05-14 16:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0002_alter_comment_image'),
    ]

    operations = [
        migrations.AlterField(
            model_name='comment',
            name='image',
            field=models.FilePathField(path='/home/josefperez/Escritorio/SSTT/TrabajoFinal/teveo/app/static/img/'),
        ),
    ]
