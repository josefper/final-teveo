from django.contrib import admin
from .models import Comment, Camera, Configuration

# Register your models here.
admin.site.register(Comment)
admin.site.register(Camera)
admin.site.register(Configuration)
