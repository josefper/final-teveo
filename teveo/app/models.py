from django.db import models
from django.conf import settings


# Create your models here.
class Camera(models.Model):
    id = models.CharField(max_length=200, primary_key=True)
    camPage = models.URLField(blank=False)
    camStatic = models.URLField(blank=False)
    camDynamic = models.URLField(blank=False)
    name = models.CharField(max_length=200)
    latitude = models.FloatField()
    longitude = models.FloatField()
    numComments = models.IntegerField(default=0)
    
    def __str__(self):
        return self.id


class Comment(models.Model):
    camId = models.URLField(blank=False)
    body = models.TextField(blank=False)
    date = models.DateTimeField("publicado")
    userName = models.CharField(max_length=200, default="Anónimo")
    image = models.FilePathField(path=f'{settings.BASE_DIR}/app/static/img/', blank=False)
    
    def __str__(self):
        return self.camId
    
    
class Configuration(models.Model):
    id = models.IntegerField(primary_key=True)
    userName = models.CharField(max_length=200)
    sizeSelect = models.CharField(max_length=50)
    fontSelect = models.CharField(max_length=50)
    
    def __str__(self):
        return self.userName
    