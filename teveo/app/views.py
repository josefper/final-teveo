import os, re, random, datetime, urllib.request
from django.http import HttpResponse, Http404, JsonResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from .models import Camera, Comment, Configuration
from xml.dom.minidom import parse
from django.conf import settings
from django.contrib import messages
from django.template import loader


# Create your views here.
def count_all():
    """Función para obtener el número de cámaras y comentarios almacenados en la base de datos"""
    return Camera.objects.count(), Comment.objects.count()


def index(request):
    """Función para mostrar la página de inicio"""
    contents = Comment.objects.order_by('-date')
    total_cameras, total_comments = count_all()
    return render(request, 'index.html', {'contents': contents, 'total_cameras': total_cameras,
                                          'total_comments': total_comments,
                                          'font_select': request.COOKIES.get('font_select'),
                                          'size_h1': request.COOKIES.get('size_h1'),
                                          'size_h2': request.COOKIES.get('size_h2'),
                                          'size_p': request.COOKIES.get('size_p')})


def get_cameras(request):
    """Función para obtener la lista de cámaras"""
    xml_path = os.path.join(settings.BASE_DIR, 'app', 'static', 'xmls', 'listado1.xml')
    raw_info = parse(xml_path)
    info = raw_info.getElementsByTagName('camara')
    for camara in info:
        identifier = camara.getElementsByTagName('id')[0].firstChild.data
        src = camara.getElementsByTagName('src')[0].firstChild.data
        lugar = camara.getElementsByTagName('lugar')[0].firstChild.data
        coordenadas = camara.getElementsByTagName('coordenadas')[0].firstChild.data
        latitud = coordenadas.split(',')[0]
        longitud = coordenadas.split(',')[1]
        num = 0
        for comment in Comment.objects.all():
            if comment.camId == request.path + "/" + identifier:
                num += 1
        new_cam = Camera(id=identifier, camPage=src, camStatic=request.path + "/" + identifier,
                         camDynamic=request.path + "/" + identifier + "-dyn", name=lugar, latitude=latitud,
                         longitude=longitud, numComments=num)
        new_cam.save()
    
    xml_path = os.path.join(settings.BASE_DIR, 'app', 'static', 'xmls', 'listado2.xml')
    raw_info = parse(xml_path)
    info = raw_info.getElementsByTagName('cam')
    for cam in info:
        identifier = cam.getAttribute('id')
        url = cam.getElementsByTagName('url')[0].firstChild.data
        lugar = cam.getElementsByTagName('info')[0].firstChild.data
        latitud = cam.getElementsByTagName('place')[0].getElementsByTagName('latitude')[0].firstChild.data
        longitud = cam.getElementsByTagName('place')[0].getElementsByTagName('longitude')[0].firstChild.data
        num = 0
        for comment in Comment.objects.all():
            if comment.camId == request.path + "/" + identifier:
                num += 1
        cam = Camera(id=identifier, camPage=url, camStatic=request.path + "/" + identifier, camDynamic=request.path +
                     "/" + identifier + "-dyn", name=lugar, latitude=latitud, longitude=longitud, numComments=num)
        cam.save()


def camaras(request):
    """Función para mostrar la página de cámaras"""
    get_cameras(request)
    cameras = Camera.objects.order_by('-numComments')
    random_choice = random.choice(cameras)
    random_cam = random_choice.camPage
    total_cameras, total_comments = count_all()
    return render(request, 'camaras.html', {'cameras': cameras, 'random_cam': random_cam,
                                            'total_cameras': total_cameras, 'total_comments': total_comments,
                                            'font_select': request.COOKIES.get('font_select'),
                                            'size_h1': request.COOKIES.get('size_h1'),
                                            'size_h2': request.COOKIES.get('size_h2'),
                                            'size_p': request.COOKIES.get('size_p')})


def show_camera(request, ident):
    """Función para mostrar la página de una cámara"""
    try:
        camera = Camera.objects.get(id=ident)
        comments = Comment.objects.order_by('-date')
        total_cameras, total_comments = count_all()
        return render(request, 'camara_.html', {'camera': camera, 'comments': comments,
                                                'total_cameras': total_cameras, 'total_comments': total_comments,
                                                'font_select': request.COOKIES.get('font_select'),
                                                'size_h1': request.COOKIES.get('size_h1'),
                                                'size_h2': request.COOKIES.get('size_h2'),
                                                'size_p': request.COOKIES.get('size_p')})
    except Camera.DoesNotExist:
        raise Http404("La camara no existe.")
    
    
def get_json(request, ident):
    """Función para obtener la información de una cámara en formato JSON"""
    try:
        camera = Camera.objects.get(id=ident)
        data = {
            'ID': camera.id,
            'Nombre': camera.name,
            'Latitud': camera.latitude,
            'Longitud': camera.longitude,
            'Número de Comentarios': camera.numComments,
            'URL de la imagen': camera.camPage,
            'Página de la Cámara': camera.camStatic,
            'Página Dinámica': camera.camDynamic
        }
        return JsonResponse(data)
    except Camera.DoesNotExist:
        raise Http404("La camara no existe.")
    
    
def img_htmx(request, ident):
    """Función para devolver un HTML con la imagen de una cámara"""
    try:
        camera = Camera.objects.get(id=ident)
        url = camera.camPage
        if url is None or url == "":
            html = '<img id="img-cam" class="mx-auto d-block" src="#">'
        else:
            html = f'<img id="img-cam" class="mx-auto d-block" src="{url}">'
        return HttpResponse(html, content_type="text/html")
    except Camera.DoesNotExist:
        raise Http404("La camara no existe.")
    
    
def com_htmx(request, ident):
    """Función para devolver un HTML con los comentarios de una cámara"""
    try:
        camera = Camera.objects.get(id=ident)
        cam_id = camera.camStatic
        comments = Comment.objects.order_by('-date')
        html = '<ul class="list-group">'
        
        if comments:
            for comment in comments:
                if comment.camId == cam_id:
                    html += (f'<li class="list-group-item"><b>@{comment.userName}</b>: {comment.body}. '
                             f'<br><b>{comment.date.strftime("%Y-%m-%d %H:%M:%S")}</b></li>')
        html += '</ul>'
        
        return HttpResponse(html, content_type="text/html")
    except Camera.DoesNotExist:
        raise Http404("La camara no existe.")
    
    
def show_dynamic(request, ident):
    """Función para mostrar la página dinámica de una cámara"""
    try:
        camera = Camera.objects.get(id=ident)
        comments = Comment.objects.order_by('-date')
        total_cameras, total_comments = count_all()
        return render(request, 'camarax.html', {'camera': camera, 'comments': comments,
                                                'total_cameras': total_cameras, 'total_comments': total_comments,
                                                'font_select': request.COOKIES.get('font_select'),
                                                'size_h1': request.COOKIES.get('size_h1'),
                                                'size_h2': request.COOKIES.get('size_h2'),
                                                'size_p': request.COOKIES.get('size_p')})
    except Camera.DoesNotExist:
        raise Http404("La camara no existe.")


def download_img(url, file_name):
    """Función para descargar una imagen"""
    
    # Construir la ruta completa del archivo
    file_path = os.path.join(settings.BASE_DIR, 'app/static/img', file_name)
    
    # Descargar la imagen
    urllib.request.urlretrieve(url, file_path)


@csrf_exempt
def comentario(request, ident):
    """Función para guardar un comentario en la base de datos"""
    try:
        camera = Camera.objects.get(id=ident)
        ind = 0
        img = f'img{camera.id}-{ind}.jpg'
        comment = Comment.objects.filter(image=img)
        
        while comment.exists():
            ind += 1
            img = f'img{camera.id}-{ind}.jpg'
            comment = Comment.objects.filter(image=img)
        
        download_img(camera.camPage, img)
        if request.method == 'POST':
            new_comment = Comment(camId=camera.camStatic, body=request.POST['comment'], date=datetime.datetime.now(),
                                  userName=request.COOKIES.get('user_name', 'Anónimo'), image=img)
            new_comment.save()
         
        comments = Comment.objects.order_by('-date')
        total_cameras, total_comments = count_all()
        return render(request, 'comentario.html', {'camera': camera, 'comments': comments,
                                                   'total_cameras': total_cameras, 'total_comments': total_comments,
                                                   'font_select': request.COOKIES.get('font_select'),
                                                   'size_h1': request.COOKIES.get('size_h1'),
                                                   'size_h2': request.COOKIES.get('size_h2'),
                                                   'size_p': request.COOKIES.get('size_p')})
    except Camera.DoesNotExist:
        raise Http404("La camara no existe.")


def helpme(request):
    """Función para mostrar la página de ayuda"""
    total_cameras, total_comments = count_all()
    return render(request, 'helpme.html', {'total_cameras': total_cameras,
                                           'total_comments': total_comments,
                                           'font_select': request.COOKIES.get('font_select'),
                                           'size_h1': request.COOKIES.get('size_h1'),
                                           'size_h2': request.COOKIES.get('size_h2'),
                                           'size_p': request.COOKIES.get('size_p')})


def check_size(size_select):
    """Función para comprobar el tamaño de los títulos y los párrafos"""
    if size_select == "Pequeña":
        size_h1 = 2
        size_h2 = 1.5
        size_p = 13
    elif "Mediana" in size_select:
        size_h1 = 2.5
        size_h2 = 2
        size_p = 16
    elif size_select == "Grande":
        size_h1 = 3
        size_h2 = 2.5
        size_p = 18
    else:
        size_h1 = 4
        size_h2 = 3.25
        size_p = 21
        
    return size_h1, size_h2, size_p


def check_session(request, id_session):
    """Función para comprobar si hay una sesión activa"""
    user_name = request.COOKIES.get('user_name', 'Anónimo')
    font_select = request.COOKIES.get('font_select', 'var(--bs-body-font-family)')
    size_h1 = request.COOKIES.get('size_h1', 2.5)
    size_h2 = request.COOKIES.get('size_h2', 2)
    size_p = request.COOKIES.get('size_p', 16)
    id_cookie = None
    if id_session != "" and id_session is not None:
        try:
            id_session = int(id_session)
            session = Configuration.objects.get(id=id_session)
            user_name = session.userName
            font_select = session.fontSelect
            size_h1, size_h2, size_p = check_size(size_select=session.sizeSelect)
            id_cookie = id_session
            
            # Agregar un mensaje de confirmación
            messages.success(request, 'La configuración se ha cargado correctamente.')
        except Configuration.DoesNotExist:
            # Agregar un mensaje de advertencia
            messages.warning(request, 'Este identificador no existe o quedó invalidado.')
        except ValueError:
            # Agregar un mensaje de error
            messages.error(request, 'El identificador debe ser un número entero.')
    return user_name, font_select, size_h1, size_h2, size_p, id_cookie


def check_config(request):
    """Función para comprobar si hay una configuración guardada"""
    user_name, font_select = "Anónimo", "var(--bs-body-font-family)"
    size_h1, size_h2, size_p = 2.5, 2, 16
    id_cookie = None
    
    if "configButton" in request.POST:
        if "userName" in request.POST:
            user_name = request.POST['userName']
            user_name = user_name.strip()
            if user_name == "" or user_name is None:
                user_name = "Anónimo"
            else:
                user_name = request.POST['userName']
        
        if "fontSelect" in request.POST:
            font_select = request.POST['fontSelect']
            if "Sans-Serif" in font_select:
                font_select = "var(--bs-body-font-family)"
        
        if "sizeSelect" in request.POST:
            size_h1, size_h2, size_p = check_size(size_select=request.POST['sizeSelect'])
            
    elif "idRecovery" in request.POST and "idChecker" in request.POST:
        id_session = request.POST.get('idChecker', None)
        user_name, font_select, size_h1, size_h2, size_p, id_cookie = check_session(request, id_session)
        
    return user_name, font_select, size_h1, size_h2, size_p, id_cookie


@csrf_exempt
def config(request):
    """Función para mostrar la página de configuración"""
    total_cameras, total_comments = count_all()
    
    if request.method == 'GET':
        user_name, font_select, size_h1, size_h2, size_p, id_cookie = check_session(request,
                                                                                    request.COOKIES.get('id_session',
                                                                                                        None))
                
        response = HttpResponse(render(request, 'config.html', {'total_cameras': total_cameras,
                                                                'total_comments': total_comments,
                                                                'font_select': font_select,
                                                                'size_h1': size_h1, 'size_h2': size_h2,
                                                                'size_p': size_p}))
        response.set_cookie('user_name', user_name)
        response.set_cookie('font_select', font_select)
        response.set_cookie('size_h1', size_h1)
        response.set_cookie('size_h2', size_h2)
        response.set_cookie('size_p', size_p)
        if id_cookie is not None:
            response.set_cookie('id_session', id_cookie)
        
    elif request.method == 'POST':
        user_name, font_select, size_h1, size_h2, size_p, id_cookie = check_config(request)
        if "idGenerator" in request.POST:
            id_session = 10000
            sessions = Configuration.objects.filter(id=id_session)
            
            # Obtener identificador único
            while sessions.exists():
                id_session = random.randint(10000, 99999)
                sessions = Configuration.objects.filter(id=id_session)
                
            user_name = request.COOKIES.get('user_name', 'Anónimo')
            font_select = request.COOKIES.get('font_select', 'var(--bs-body-font-family)')
            size_h1 = request.COOKIES.get('size_h1', 2.5)
            size_h2 = request.COOKIES.get('size_h2', 2)
            size_p = request.COOKIES.get('size_p', 16)
            
            if int(size_p) == 13:
                size_select = "Pequeña"
            elif int(size_p) == 16:
                size_select = "Mediana"
            elif int(size_p) == 18:
                size_select = "Grande"
            else:
                size_select = "Muy Grande"
                
            print(user_name, font_select, size_h1, size_h2, size_p, id_cookie)
            # Guardar la configuración
            session = Configuration(id=id_session, userName=user_name, fontSelect=font_select,
                                    sizeSelect=size_select)
            session.save()
            id_cookie = id_session
            messages.success(request, 'Tu ID de sesión es ' + str(id_cookie))
            
        response = HttpResponse(render(request, 'config.html', {'total_cameras': total_cameras,
                                                                'total_comments': total_comments,
                                                                'font_select': font_select,
                                                                'size_h1': size_h1, 'size_h2': size_h2,
                                                                'size_p': size_p}))
        response.set_cookie('user_name', user_name)
        response.set_cookie('font_select', font_select)
        response.set_cookie('size_h1', size_h1)
        response.set_cookie('size_h2', size_h2)
        response.set_cookie('size_p', size_p)
        if id_cookie is not None:
            response.set_cookie('id_session', id_cookie)
    else:
        response = HttpResponse(render(request, 'config.html', {'total_cameras': total_cameras,
                                                                'total_comments': total_comments,
                                                                'font_select': request.COOKIES.get('font_select'),
                                                                'size_h1': request.COOKIES.get('size_h1'),
                                                                'size_h2': request.COOKIES.get('size_h2'),
                                                                'size_p': request.COOKIES.get('size_p')}))
    return response
