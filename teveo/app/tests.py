from django.test import TestCase
from .models import Camera


# Create your tests here.
class AppTest(TestCase):
    def test_index(self):
        """Comprobar que la página principal funciona correctamente."""
        response = self.client.get('/app/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'index.html', 'base.html')
        
    def test_cameras(self):
        """Comprobar que la página de cámaras funciona correctamente."""
        response = self.client.get('/app/camaras')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'camaras.html', 'base.html')
        num_cameras = Camera.objects.count()
        self.assertEqual(num_cameras, 7)
        
    def test_camera(self):
        """Comprobar que la página de una cámara funciona correctamente."""
        for camera in Camera.objects.all():
            response = self.client.get('/app/camaras/' + camera.id)
            self.assertEqual(response.status_code, 200)
            self.assertTemplateUsed(response, 'camara_.html', 'base.html')
            self.assertTemplateNotUsed(response, 'camarax.html')
            
    def test_camerax(self):
        """Comprobar que la página de una cámara dinámica funciona correctamente."""
        for camera in Camera.objects.all():
            response = self.client.get('/app/camaras/' + camera.id + '-dyn')
            self.assertEqual(response.status_code, 200)
            self.assertTemplateUsed(response, 'camarax.html', 'base.html')
            self.assertTemplateNotUsed(response, 'camara_.html')
        
    def test_json(self):
        """Comprobar que la página de una cámara en formato JSON funciona correctamente."""
        for camera in Camera.objects.all():
            response = self.client.get('/app/camaras/' + camera.id + '/json')
            self.assertEqual(response.status_code, 200)
            self.assertEqual(response['Content-Type'], 'application/json')
            
    def test_img_htmx(self):
        """Comprobar que etiqueta dinámica de una imagen funciona correctamente."""
        for camera in Camera.objects.all():
            response = self.client.get('/app/camaras/' + camera.id + '-dyn/img-htmx')
            self.assertEqual(response.status_code, 200)
            self.assertEqual(response['Content-Type'], 'text/html')
            
    def test_com_htmx(self):
        """Comprobar que los comentarios dinámicos de una imagen funciona correctamente."""
        for camera in Camera.objects.all():
            response = self.client.get('/app/camaras/' + camera.id + '-dyn/com-htmx')
            self.assertEqual(response.status_code, 200)
            self.assertEqual(response['Content-Type'], 'text/html')
            
    def test_comentario(self):
        """Comprobar que los comentarios funcionan correctamente."""
        for camera in Camera.objects.all():
            response = self.client.get('/app/camaras/' + camera.id + '/comentario')
            self.assertEqual(response.status_code, 200)
            self.assertTemplateUsed(response, 'comentario.html', 'base.html')
            response = self.client.post('/app/camaras/' + camera.id + '/comentario', {'comment': 'Este comentario es de prueba'})
            self.assertEqual(response.status_code, 200)
            self.assertTemplateUsed(response, 'comentario.html', 'base.html')
    
    def test_helpme(self):
        """Comprobar que la página de ayuda funciona correctamente."""
        response = self.client.get('/app/ayuda')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'helpme.html', 'base.html')
        
    def test_config(self):
        """Comprobar que la página de configuración funciona correctamente."""
        response = self.client.get('/app/config')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'config.html', 'base.html')
        response = self.client.post('/app/config', {'userName': 'yo me llamo ralph', 'sizeSelect': 'Muy Grande',
                                                    'fontSelect': 'Arial'})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'config.html', 'base.html')
        
    def test_not_found(self):
        """Comprobar que la página de error 404 funciona correctamente."""
        response = self.client.get('/app/algoinventado')
        self.assertEqual(response.status_code, 404)
        response = self.client.get('/app/camaras/algoinventado')
        self.assertEqual(response.status_code, 404)
        response = self.client.get('/app/camaras/algoinventado/comentario')
        self.assertEqual(response.status_code, 404)
        